package ru.bobday.geolocationservice.models;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class CountryModel {
    private int id;
    private String name;
    private Set<String> topLevelDomain;
    private long population;
    private Double[] latlng;
    private CurrencyModel[] currencies;
    private LanguageModel[] languages;
    private TranslationModel translations;
    private String flag;
    private RegionalBlocsModel[] regionalBlocs;
}
