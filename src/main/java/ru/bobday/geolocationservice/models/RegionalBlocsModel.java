package ru.bobday.geolocationservice.models;

import lombok.Data;

@Data
public class RegionalBlocsModel {
    private String acronym;
    private String name;
    private String otherAcronyms;
    private String otherNames;
}
