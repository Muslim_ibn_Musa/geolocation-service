package ru.bobday.geolocationservice.models;

import lombok.Data;

@Data
public class TranslationModel {
    private String de;
    private String es;
    private String fr;
    private String ja;
    private String it;
    private String br;
    private String pt;
    private String nl;
    private String hr;
    private String fa;
}
