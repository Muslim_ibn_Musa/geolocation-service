package ru.bobday.geolocationservice.models;

import lombok.Data;

@Data
public class CurrencyModel {
    private String code;
    private String name;
    private String symbol;
}
