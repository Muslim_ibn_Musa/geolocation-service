package ru.bobday.geolocationservice.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.bobday.geolocationservice.models.CountryModel;

@Repository
public interface GeolocationRepository extends CrudRepository<CountryModel, Integer> {

}
