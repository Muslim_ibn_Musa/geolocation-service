package ru.bobday.geolocationservice.services.impl;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.bobday.geolocationservice.models.CountryModel;
import ru.bobday.geolocationservice.services.GeolocationService;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class GeolocationServiceImpl implements GeolocationService {

    final RestTemplate restTemplate;

    @Value("${rest-countries-all-url}")
    private String restCountriesApiUrl;

    public GeolocationServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostConstruct
    public void getAndSaveCountriesFromApi() {
        ResponseEntity<List<CountryModel>> response
                = restTemplate.getForEntity(restCountriesApiUrl, CountryModel.class);

        List<CountryModel> resp = restTemplate.exchange(restCountriesApiUrl, HttpMethod.GET, null, )

        System.out.println(new Gson().toJson(response.getBody()));
    }

    public CountryModel getCountry() {


        return new CountryModel();
    }

}
